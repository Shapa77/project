public class User
{
	protected int id_User;

	public User() { }
	public User(int ID)
	{
		this.id_User = ID;
	}
	public virtual int id_User
	{
		get {return id_User; }
		set {id_User = value; }
	}
}

public class CardNumber
{
	protected string Number;
	protected string Data;
	protected string CVV;
	public CardNumber() { }
	public CardNumber(string Number, string Data, string CVV)
	{
		this.Number = Number;
		this.Data = Data;
		this.CVV = CVV;
	}
	public virtual string Number
	{
		get {return Number; }
		set {Number = value; }
	}
	public virtual string Data
	{
		get {return Data; }
		set {Data = value; }
	}
	public virtual string CVV
	{
		get {return CVV; }
		set {CVV = value; }
	}

}

public class AutorizedUser : User
{
	protected string Number;
	protected string Password;
	protected string Name;
	protected string Surname;
	protected CardNumber Card;
	public AutorizedUser() { }
	public AutorizedUser(string Number, string Password, string Name, string Surname, CardNumber Card)
	{
		this.Number = Number;
		this.Password = Password;
		this.Name = Name;
		this.Surname = Surname;
		this.Card = Card;
	}
	public virtual string Number
	{
		get {return Number; }
		set {Number = value; }
	}
	public virtual string Password
	{
		get {return Password; }
		set {Password = value; }
	}
	public virtual string Name
	{
		get {return Name; }
		set {Name = value; }
	}
	public virtual string Surname
	{
		get {return Surname; }
		set {Surname = value; }
	}
	public virtual CardNumber Card
	{
	get {return Card; }
	set {Card = value; }
	}
}

public class Tariffs
{
	protected string Description;
	protected double Price;
	public Tariffs() { }
	public Tariffs(string Description, double Price)
	{
		this.Description = Description;
		this.Price = Price;
	}
	public virtual string Description
	{
		get {return Description; }
		set {Description = value; }
	}
	public virtual double Price
	{
		get {return Price; }
		set {Price = value; }
	}

}

public class Number
{
	protected int id_Number;
	protected AutorizedUser User;
	protected double Balance;
	protected Tariffs Tariff;
	public Number() { }
	public Number(int id_Number, AutorizedUser User, double Balance, Tariffs Tariff)
	{
		this.id_Number = id_Number;
		this.User = User;
		this.Balance = Balance;
		this.Tariff = Tariff;
	}
	public virtual int id_Number
	{
		get {return id_Number; }
		set {id_Number = value; }
	}
	public virtual AutorizedUser User
	{
		get {return User; }
		set {User = value; }
	}
	public virtual double Balance
	{
		get {return Balance; }
		set {Balance = value; }
	}
	public virtual Tariffs Tariff
	{
		get {return Tariff; }
		set {Tariff = value; }
	}
}

public class TechSupport
{
	protected AutorizedUser User;
	protected string Number;
	protected string ExtraNumber;
	protected string Description;
	public TechSupport() { }
	public TechSupport(AutorizedUser User, string Number, string ExtraNumber, string Description)
	{
		this.User = User;
		this.Number = Number;
		this.ExtraNumber = ExtraNumber;
		this.Description = Description;
	}
	public virtual AutorizedUser User
	{
		get {return User; }
		set {User = value; }
	}
	public virtual string Number
	{
		get {return Number; }
		set {Number = value; }
	}
	public virtual string ExtraNumber
	{
		get {return ExtraNumber; }
		set {ExtraNumber = value; }
	}
	public virtual string Description
	{
		get {return Description; }
		set {Description = value; }
	}
}

public class Operations
{
	protected int id_Opertion;
	protected string Description;
	protected string Number;
	public Operations() { }
	public Operations(int id_Opertion, string Description, string Number)
	{
		this.id_Opertion = id_Opertion;
		this.Description = Description;
		this.Number = Number;
	}
	public virtual int id_Opertion
	{
		get {return id_Opertion; }
		set {id_Opertion = value; }
	}
	public virtual string Description
	{
		get {return Description; }
		set {Description = value; }
	}
	public virtual string Number
	{
		get {return Number; }
		set {Number = value; }
	}
}

public class BalanceOperations : Operations
{
	protected double Summa;
	protected string Date;
	public BalanceOperations() { }
	public BalanceOperations(double Summa, string Date)
	{
		this.Summa = Summa;
		this.Date = Date;
	}
	public virtual double Summa
	{
		get {return Summa; }
		set {Summa = value; }
	}
	public virtual string Date
	{
		get {return Date; }
		set {Date = value; }
	}
}